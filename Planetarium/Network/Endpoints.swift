//
//  Endpoints.swift
//  Planetarium
//
//  Created by Sha'Marcus Walker on 8/17/23.
//

import Foundation
struct Endpoint<Response: Decodable> {
    let url: URL
    let responseType: Response.Type
}

class Endpoints {
    
    static func fetchPlanets() -> Endpoint<SolarSystem> {
        Endpoint(url: URL(string: "https://swapi.dev/api/planets")!, responseType: SolarSystem.self)
    }
    
}
