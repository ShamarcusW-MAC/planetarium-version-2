//
//  NetworkManager.swift
//  Planetarium
//
//  Created by Sha'Marcus Walker on 8/17/23.
//

import Foundation
import Combine

protocol NetworkProtocol {
    func fetchData<Response>(endpoint: Endpoint<Response>) -> AnyPublisher<Response, Error> where Response: Decodable
}

class NetworkManager: NetworkProtocol {
    
    private let jsonDecoder = JSONDecoder()
    private let urlSession: URLSession
    
    init(urlSession: URLSession = .shared) {
        self.urlSession = urlSession
    }
    
    func fetchData<Response>(endpoint: Endpoint<Response>) -> AnyPublisher<Response, Error> where Response : Decodable {

        urlSession.dataTaskPublisher(for: endpoint.url)
            .tryMap { data, response in
                guard let httpResponse = response as? HTTPURLResponse,
                      200..<300 ~= httpResponse.statusCode else {
                    throw URLError(.badServerResponse)
                }
                
                return data
            }
            .decode(type: Response.self, decoder: jsonDecoder)
            .eraseToAnyPublisher()
        
    }
}
